# **About** 

As an art and technology enthousiast, my career in the film/commercial and gaming industry has spanned over
two decades. I am dedicated to work with directors, producers and interactive storytellers to fulfill every visual
aspect on a project, from the creative approach, technologies used and bugdet constrains. My expertise span
from the beginning stages of development to the final delivery of visual effects projects.

# **Professional Skills**

+ Excellent knowledge about composition, cinematic design and animation timing.
+ Excellent understanding of Directing/Lighting/Camera work/Practical Effects/Camera Gripping/Art
  Department/Production Design and Props.
+ Extensive experience in script vfx breakdown.
+ Extensive on-set supervision experience, including data wrangling.
+ Thorough understanding of the CG & 2D principles and pipeline.
+ Excellent communication, organizational, budgetary and financial management skills.
+ Ability to work to strict deadlines.
+ Expert in industry standard software packages such as **The Foundry Nuke, Autodesk 3dsmax, Blender, Davinci Resolve, Autodesk Shotgun.**